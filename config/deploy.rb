set :user, 'railscap' 
set :domain, 'p2fx.net' 
set :application, 'p2fx' 
# adjust if you are using RVM, remove if you are not 
$:.unshift(File.expand_path('./lib', ENV['rvm_path'])) 
require "rvm/capistrano" 
set :rvm_ruby_string, '1.9.2' 
set :rvm_type, :user 
# file paths 

set :repository,  "https://e911miri@bitbucket.org/e911miri/p2fxrails.git"
set :deploy_to, "/var/www/railsapps/p2fx"

set :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :app, domain 
role :web, domain 
role :db, domain, :primary => true

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end