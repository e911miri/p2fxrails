class AddCategoryContactDistanceLocationToSip < ActiveRecord::Migration
  def change
    add_column :sips, :name, :string, default: "Some Sip Profile"
    add_column :sips, :category, :string, default: "qube"
    add_column :sips, :contact, :string, default: ""
    add_column :sips, :distance, :string, default: ""
    add_column :sips, :location, :string, default: "[0.0, 0.0]"
  end
end
