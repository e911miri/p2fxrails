class AddIqubersToUsers < ActiveRecord::Migration
  def change
    add_column :users, :iquber, :string, default: "friend"
  end
end
