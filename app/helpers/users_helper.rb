module UsersHelper
  require 'net/http'
  require 'json'
  # Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(user, options = { size: 50 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar = 
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end
  
  def push_user(user)
    uri = URI('http://p2mu.net:8058/register')
    params = { :msisdn => user.phone}
    uri.query = URI.encode_www_form(params)
    
    res = Net::HTTP.get_response(uri)
    usr = JSON.parse res.body if res.is_a?(Net::HTTPSuccess)
    puts usr
  end
end
