module SipsHelper
  require 'net/http'
  require 'json'
  P2FXBE = "http://p2mu.net:8058"

  def machinize_location(loc)
    return [10.185, 10.69]
  end

  def push_sip(sip)
    uri = URI(P2FXBE + "/sip")
    lng,lat=sip.location.split(",")
    params = { :lon => 10.69,:lat => 10.185, :contact => sip.contact, :distance => sip.distance,
      :name => sip.name, :category => sip.category}
    uri.query = URI.encode_www_form(params)
    uri.inspect
    begin
      res = Net::HTTP.get_response(uri)
    rescue Exception
      return nil
    end    
    return res.body if res.is_a?(Net::HTTPSuccess)
  end

  def get_remote_profiles(lat_lng, current_user)
    uri = URI(P2FXBE + "/sip")
    lat, lon=lat_lng
    params={:lat => lat, :lon => lon, :msisdn => current_user.phone}
    uri.query = URI.encode_www_form(params)
    
    print uri.inspect
    begin
      res = Net::HTTP.get_response(uri)
    rescue Exception
      return nil
    end
    
    return JSON.parse(res.body) if res.is_a?(Net::HTTPSuccess)
  end
end