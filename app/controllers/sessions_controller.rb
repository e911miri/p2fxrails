class SessionsController < ApplicationController
  respond_to :html, :json
  def new
    respond_with session
  end

  def create    
    user = User.find_by_email(params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      sign_in user
      # redirect_back_or user
    else
      flash.now[:error] = 'Invalid email/password combination'
      # render 'new'    
    end
    respond_with session
  end
  def destroy
    sign_out
    respond_with session
    # redirect_to root_path
  end
  
  private 
  
  helper_method :session
  
  def session
    # If the action is new or create...
    if @user = params[:action] =~ /new/
      User.new#(params[:user])
    elsif @user = params[:action] =~ /destroy/
      User.new
    elsif @user = params[:action] =~ /create/
      if params[:error]
        params[:error]
        User.new
      else        
        # redirect_back_or User.new params[:user]
        # User.new params[:user]
        begin
          emal = params[:user][:email]
        rescue Exception
          emal = params[:email]
        end
        User.find_by_email(emal)
      end
      # User.new(params[:user])
    else
      User.find(params[:id])
    end
  end
end