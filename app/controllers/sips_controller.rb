class SipsController < ApplicationController
  respond_to :html, :json, :xml
  before_filter :auhenticate_user! 
  def index
  end

  def search
    # if cookies[:lat_lng]
      # lat_lng = cookies[:lat_lng]
    # else 
      # lat_lng = "10.185|10.69"
    # end
    # @lat_lng = lat_lng.split("|")
    # @sip_profiles = get_remote_profiles(@lat_lng, current_user)
#     
    # respond_with(@sips = sip.sear) 
    query=params["query"]
    @query= query
  end
  
  def new
    @sip= Sip.new
  end

  def create
    @sip = Sip.new(params[:sip])
    @sip.contact= current_user.phone
    if @sip.save
      push_sip @sip
      flash[:success] = "Sip Profile Saved!!"
      redirect_to @sip
    else
      render 'new'
    end
  end
  
  def show
    @sip = Sip.find(params[:id])
  end
end
