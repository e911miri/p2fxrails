class Sip < ActiveRecord::Base
  # self.site = "http://api.people.com:3000"
  attr_accessible :category, :contact, :distance, :location, :name
  
  validates :name, presence: true, length: { maximum: 140 }
  belongs_to :user
  
  def search(lat_lng)
    return get_remote_profiles(lat_lng, currentuser)
  end
end
